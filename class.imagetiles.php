<?php
//---------------------------------------------------------------------------------------//
//  Class: ImageTiles
//	
//  Description: This class accepts a path to an image and creates a set of tiles
//							 from that image. The purpose is to replace images on a website
//							 with a series of small tiles. The user will see the full image,
//							 but if they try to hotlink the image, or save the image, they 
//							 will only get a small tile. The user would have to download all
//							tiles in order to replicate the full image.
//
//	Usage: Works with any jpg, gif or png image. Refer to README file included with
//				this package for examples, or go to http://supersimple.com/imagetiles/
//				* Your images must be in a full read/write directory (permissions: 0777)
//
//	Bugs: None known.
//
//	Security: Notice that your source code will contain the path to the fullsize image
//						This is done intentionally for browsers with JS disabled. There are very
//						few ways to protect an image from ever being downloaded. This offers a level
//						of security that is beyond many users potential to break, and provides an
//						inconvenience for many others.
//
//---------------------------------------------------------------------------------------//

ini_set("memory_limit","100M"); //since image manipulation requires some memory, we increase the limit a little :D
require_once('settings.php'); //get your global settings.

class ImageTiles{
	
	var $origFile;
	var $origWidth;
	var $origHeight;
	var $type;
	var $fExt;
	var $_fnpre;
	
	var $savepath = '';
	var $URL = '';
	
	var $tileRows = 10;
	var $tileCols = 10;
	
	var $totaltiles;
	
	var $tileWidth;
	var $tileHeight;
	
	var $jpgQuality = 100;
	var $source;
	var $response;
	
	var $watermark = false;
	
	var $watermarksource;
	var $wmarkpath = 'images/watermark.png';
	var $wmarkx = 0;
	var $wmarky = 0;
	var $wmarkw = 79;
	var $wmarkh = 11;
	var $wmarkopacity = 80;
	
	function __construct($origFile){

		global $CONFIG;
		$this->savepath = $CONFIG['savepath'];
		$this->URL = $CONFIG['url'];
		$this->tileRows = $CONFIG['tilerows'];
		$this->tileCols = $CONFIG['tilecols'];
		
		$this->watermark = $CONFIG['show_watermark'];
		$this->wmarkpath = $CONFIG['watermark_path'];
		$this->wmarkopacity = $CONFIG['watermark_opacity'];
		
		if(substr($origFile,0,4) == 'http'){
			//take out the base path
			$this->origFile = str_replace($this->URL,'',$origFile);
		}else{
			$this->origFile = $origFile;
		}
		
		//determine basic settings
		$this->getSettings();
		
	}
	
	function getSettings(){
		//hold off on this for now
		$this->getInfo();
	}
	
	function getInfo(){
		list($this->origWidth, $this->origHeight, $this->type) = getimagesize($this->savepath.$this->origFile);
			switch ($this->type){
					case 1:
					$this->fExt = 'gif';
					break;
					case 2:
					$this->fExt = 'jpg';
					break;
					case 3:
					$this->fExt = 'png';
					break;
				}
		
		//get the new tile dimensions
		$this->getTileDimensions();
				
	}
	
	function getTileDimensions(){
		
		// we will need to get tiles that are in full pixels - might want to test for some basic divisors (3,4,5,7)
		if($this->origWidth % $this->tileCols == 0){
			//good to go - all is even
		}else if($this->is_prime($this->origWidth)){
			//this is a prime number - just set the columns to 1
			$this->tileCols = 1;
		}else{
			//not a prime number, but not evenly divisible by our current value
			$this->tileCols = $this->findDivisor($this->origWidth,$this->tileCols);
		}
		$this->tileWidth = $this->origWidth / $this->tileCols;
		
		if($this->origHeight % $this->tileRows == 0){
			//good to go - all is even
		}else if($this->is_prime($this->origHeight)){
			//this is a prime number - just set the columns to 1
			$this->tileRows = 1;
		}else{
			//not a prime number, but not evenly divisible by our current value
			$this->tileRows = $this->findDivisor($this->origHeight,$this->tileRows);
		}
		$this->tileHeight = $this->origHeight / $this->tileRows;
		
		
		//start making tiles now
		$this->makeTiles();
		
	}
	
	 // is_prime(number) checks if a number is a prime number or not
	function is_prime($i){
		if($i % 2 != 1) return false;
		$d = 3;
		$x = sqrt($i);
		while ($i % $d != 0 && $d < $x) $d += 2;
		return (($i % $d == 0 && $i != $d) * 1) == 0 ? true : false;
		}
	
	//function that finds a divisor
	function findDivisor($i,$d){
		while($i % $d != 0) $d--;
		return $d;
	}
	
	function makeTiles(){
		
		switch ($this->type){
				case 1:
				$this->source = imagecreatefromgif($this->savepath.$this->origFile);
				break;
				case 2:
				$this->source = imagecreatefromjpeg($this->savepath.$this->origFile);
				break;
				case 3:
				$this->source = imagecreatefrompng($this->savepath.$this->origFile);
				break;
			}
		
		//we need to loop for each row and column to get all the tiles made
		$this->totaltiles = $this->tileRows * $this->tileCols;
		
		//generate the filename prefix
		$this->_fnpre = substr($this->origFile,0,-4);
		$this->response = $this->totaltiles;
		for($i=1;$i<=$this->totaltiles;$i++){
			
			//determine if this image has already been tiled
			if(file_exists($this->savepath.$this->_fnpre.$i.'.'.$this->fExt)){ return false; }
			
			// Load new image
			$_tile = imagecreatetruecolor($this->tileWidth, $this->tileHeight);
			
			//determine the x and y offsets based on which tile we are on
			$_currRow = ceil($i/$this->tileCols);
			$_currCol = ceil($i%$this->tileCols);
			//if currCol == 0, this is our last column
			if($_currCol == 0){$_currCol = $this->tileCols;}
			
			$_offsetX = ($_currCol -1) * $this->tileWidth;
			$_offsetY = ($_currRow -1) * $this->tileHeight;
			// Resize
			imagecopyresampled($_tile, $this->source, 0, 0, $_offsetX, $_offsetY, $this->tileWidth, $this->tileHeight, $this->tileWidth, $this->tileHeight);
			
			//move file (check for file extension)
			switch ($this->type){
					case 1:
					imagegif($_tile, $this->savepath.$this->_fnpre.$i.'.'.$this->fExt);
					break;
					case 2:
					imagejpeg($_tile, $this->savepath.$this->_fnpre.$i.'.'.$this->fExt, $this->jpgQuality);
					break;
					case 3:
					imagepng($_tile, $this->savepath.$this->_fnpre.$i.'.'.$this->fExt);
					break;
			}
			
			//clean out the memory
			imagedestroy($_tile);
			
	} //end for loop
		
		//if watermark is true, go to the next operation
		if($this->watermark){ $this->makeWatermark(); }
		
	}
	
	
	//optional function to make a watermark on the source image
	function makeWatermark(){
		//get the watermark file
		$this->watermarksource = @imagecreatefrompng($this->savepath.$this->wmarkpath);
		
		//get some metadata from the watermark image(width, height) 
		$this->wmarkw = imagesx($this->watermarksource);
		$this->wmarkh = imagesy($this->watermarksource);
		
		//we are going to make the watermark sit 5px from the right-bottom corner, so we need to determine where it starts
		$this->wmarkx = $this->origWidth - $this->wmarkw - 5;
		$this->wmarky = $this->origHeight - $this->wmarkh - 5;
		
		imagecopymerge($this->source, $this->watermarksource,$this->wmarkx,$this->wmarky,0,0,$this->wmarkw,$this->wmarkh,$this->wmarkopacity);

			switch ($this->type){
					case 1:
					imagegif($this->source,$this->savepath.$this->origFile);
					break;
					case 2:
					imagejpeg($this->source,$this->savepath.$this->origFile);
					break;
					case 3:
					imagepng($this->source,$this->savepath.$this->origFile);
					break;
			}
	}
	
}//end class



$_obj = new ImageTiles($_POST['image']);

$_ret = $_obj->origWidth.',';
$_ret .= $_obj->origHeight.',';
$_ret .= $_obj->_fnpre.',';
$_ret .= $_obj->fExt.',';
$_ret .= $_obj->totaltiles;

echo $_ret;

?>