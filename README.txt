README for ImageTiles

Facts:
- ImageTiles is a script developed by Super Simple�.
- The purpose is to make your web images harder to download, copy and hotlink.
- ImageTiles is made up of 2 php files (settings.php, class.imagetiles.php) and 2 javascript files (imagetiles.js and jquery). There is also 1 html file which is just for the purpose of show example usage.
- ImageTiles uses jQuery to make the javascript simpler. Jquery was not developed by Super Simple™, but it is included in this package since it is required. Please visit http://jquery.com for details on jQuery. It is a fantastic js framework and if you have not already used it, you should try it out. The js code in this package can be translated to another framework if so desired. Our use of jQuery reflects a personal preference.
- ImageTiles uses the php GD Library. Therefore, you need php4.3+ in order to use it.

FAQs:
- q. What does this do exactly?
	a. ImageTiles will turn all the images on your page into a series of small tiles, thus making it harder for anyone to save your images or hotlink them. ie. It turns your 200x300 image into 30 tiles, each 40x50px.
	
- q. How do I make it work?
	a. Install the files into your web directory and add 'class="protect"' inside any img tags which you would like to protect.

- q. I installed everything, but my images are empty now.
	a. Most likely, you did not change the permissions on your images directory. It needs to be set to 0777 in order to work correctly.
	
License:
This script is released under the GNU license. It can be used for any purpose public or private. We ask that you preserve our comments and an attribution to Super Simple�

Please see the index.html file for sample usage of this script.

Email info@supersimple.org with any questions/comments.


