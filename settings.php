<?php
// Settings for image tiles class -----
// set the number of rows and columns in your image tile grid. (ie. 7rows x 7cols will yield 49 small tiles)
// *The numbers generally work best in the 3-9 range, but you can choose any number you like.
$CONFIG['tilerows'] = 7; 
$CONFIG['tilecols'] = 7;

// there is an option to watermark your source image, so that it cannot be copied from your html source and used. By default, it is turned off. You may turn it on by setting show_watermark=true. Also, there is a default watermark.png image included. You may replace that with your own image of any size. You also have the option to set the opacity (percentage) of the watermark.
$CONFIG['show_watermark'] = false;
$CONFIG['watermark_path'] = 'images/watermark.png';
$CONFIG['watermark_opacity'] = 80;

// the vars below should be set automatically by your server. Only modify these if you really know what you are doing --
$CONFIG['savepath'] = str_replace(basename($_SERVER['REQUEST_URI']),'',$_SERVER['SCRIPT_FILENAME']);
$CONFIG['url'] = 'http://'.$_SERVER['SERVER_NAME'].str_replace(basename($_SERVER['REQUEST_URI']),'',$_SERVER['REQUEST_URI']);
?>