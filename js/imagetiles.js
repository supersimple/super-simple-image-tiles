function makeTiles(){
	var pImages = $(".protect");
	for(var i=0;i<pImages.length;i++){
		pImages[i].id = 'protect'+i;
		generateTiles(pImages[i].src,i);
	}
}

function generateTiles(src, iter){
	$.ajax({
	   type: "POST",
	   url: "class.imagetiles.php",
	   data: "image="+src,
	   success: function(msg){
	    //alert( "Data Saved: " + msg );
			var pars = msg.split(",");
			html = '<div style="width:'+pars[0]+'px;height:'+pars[1]+'px;" class="protected">';
			for(var i=1;i<=pars[4];i++){
				html += '<img src="'+pars[2]+''+i+'.'+pars[3]+'" style="float:left;" alt="" />';
			}
			html += '</div>';
			
			$("#protect"+iter).replaceWith(html);
		}
	 });
}